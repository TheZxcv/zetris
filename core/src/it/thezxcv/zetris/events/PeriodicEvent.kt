package it.thezxcv.zetris.events

class PeriodicEvent(private val period: Float) {
    private var elapsed = 0f

    fun update(delta: Float, action: (Float) -> Unit) {
        elapsed += delta
        while (elapsed >= period) {
            action(delta)
            elapsed -= period
        }
    }

    fun reset() {
        elapsed = 0f
    }
}