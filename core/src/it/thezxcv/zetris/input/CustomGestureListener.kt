package it.thezxcv.zetris.input

import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Vector2

class CustomGestureListener : GestureDetector.GestureListener {
    var onTouchDown: (x: Float, y: Float, pointer: Int, button: Int) -> Boolean = { _, _, _, _ -> false }

    var onTap: (x: Float, y: Float, count: Int, button: Int) -> Boolean = { _, _, _, _ -> false }

    var onLongPress: (x: Float, y: Float) -> Boolean = { _, _ -> false }

    var onFling: (velocityX: Float, velocityY: Float, button: Int) -> Boolean = { _, _, _ -> false }

    var onPan: (x: Float, y: Float, deltaX: Float, deltaY: Float) -> Boolean = { _, _, _, _ -> false }

    var onPanStop: (x: Float, y: Float, pointer: Int, button: Int) -> Boolean = { _, _, _, _ -> false }

    var onZoom: (initialDistance: Float, distance: Float) -> Boolean = { _, _ -> false }

    var onPinch: (initialPointer1: Vector2?, initialPointer2: Vector2?, pointer1: Vector2?, pointer2: Vector2?) -> Boolean = { _, _, _, _ -> false }

    var onPinchStop: () -> Unit = {}

    override fun touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean {
        return onTouchDown(x, y, pointer, button)
    }

    override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean {
        return onTap(x, y, count, button)
    }

    override fun longPress(x: Float, y: Float): Boolean {
        return onLongPress(x, y)
    }

    override fun fling(velocityX: Float, velocityY: Float, button: Int): Boolean {
        return onFling(velocityX, velocityY, button)
    }

    override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float): Boolean {
        return onPan(x, y, deltaX, deltaY)
    }

    override fun panStop(x: Float, y: Float, pointer: Int, button: Int): Boolean {
        return onPanStop(x, y, pointer, button)
    }

    override fun zoom(initialDistance: Float, distance: Float): Boolean {
        return onZoom(initialDistance, distance)
    }

    override fun pinch(initialPointer1: Vector2?, initialPointer2: Vector2?, pointer1: Vector2?, pointer2: Vector2?): Boolean {
        return onPinch(initialPointer1, initialPointer2, pointer1, pointer2)
    }

    override fun pinchStop() {
        onPinchStop()
    }
}