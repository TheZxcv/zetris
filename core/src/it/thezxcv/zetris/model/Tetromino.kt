package it.thezxcv.zetris.model

import com.badlogic.gdx.math.MathUtils

enum class TetrominoShape(val shapes: Array<Array<IntArray>>) {
    I(arrayOf(
            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(1, 1, 1, 1),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(1, 1, 1, 1),
                    intArrayOf(0, 0, 0, 0),
            ),
    )),
    O(arrayOf(
            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
            ),
    )),
    T(arrayOf(
            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(1, 1, 1, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(1, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),
    )),
    J(arrayOf(
            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(1, 1, 1, 0),
                    intArrayOf(0, 0, 1, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(1, 0, 0, 0),
                    intArrayOf(1, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),
    )),
    L(arrayOf(
            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(1, 1, 1, 0),
                    intArrayOf(1, 0, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 0, 1, 0),
                    intArrayOf(1, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
            ),
    )),
    S(arrayOf(
            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(1, 0, 0, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(1, 0, 0, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),
    )),
    Z(arrayOf(
            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(1, 0, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 0, 0, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(0, 1, 1, 0),
                    intArrayOf(0, 0, 0, 0),
            ),

            arrayOf(
                    intArrayOf(0, 1, 0, 0),
                    intArrayOf(1, 1, 0, 0),
                    intArrayOf(1, 0, 0, 0),
                    intArrayOf(0, 0, 0, 0),
            ),
    )),
}

enum class ShapeColor {
    BLUE,
    GREEN,
    PINK,
    PURPLE,
    YELLOW,
}

fun randomColor(): ShapeColor {
    val i = MathUtils.random(0, ShapeColor.values().size - 1)
    return ShapeColor.values()[i]
}

fun randomShape(): TetrominoShape {
    val i = MathUtils.random(0, TetrominoShape.values().size - 1)
    return TetrominoShape.values()[i]
}

class Tetromino(
        val x: Int,
        val y: Int,
        val shape: TetrominoShape,
        val color: ShapeColor,
        val rotation: Int = 0) {
    fun rotate() = Tetromino(x, y, shape, color, (rotation + 1) % 4)

    fun right() = Tetromino(x + 1, y, shape, color, rotation)

    fun left() = Tetromino(x - 1, y, shape, color, rotation)

    fun down() = Tetromino(x, y + 1, shape, color, rotation)

    fun grid() = shape.shapes[rotation]
}