package it.thezxcv.zetris.model

import it.thezxcv.zetris.model.Constants.COLUMNS
import it.thezxcv.zetris.model.Constants.ROWS

class Board {
    val cells = MutableList(ROWS) {
        IntArray(COLUMNS)
    }

    fun checkCollision(tetromino: Tetromino): Boolean {
        var currX = tetromino.x
        var currY = tetromino.y
        for (row in tetromino.grid()) {
            for (cell in row) {
                if (currY >= 0 && currY < cells.size
                        && currX >= 0 && currX < cells[0].size
                        && !isEmpty(cell) && !isEmpty(cells[currY][currX])) {
                    return true
                }
                currX++
            }
            currX = tetromino.x
            currY++
        }

        currX = tetromino.x
        currY = tetromino.y
        for (row in tetromino.grid()) {
            for (cell in row) {
                if ((currY < 0 || currY >= cells.size
                                || currX < 0 || currX >= cells[0].size)
                        && !isEmpty(cell)) {
                    return true
                }
                currX++
            }
            currX = tetromino.x
            currY++
        }

        return false
    }

    private fun isEmpty(cell: Int) = cell == 0

    fun add(tetromino: Tetromino) {
        var currX = tetromino.x
        var currY = tetromino.y
        for (row in tetromino.grid()) {
            for (cell in row) {
                if (cell == 1 && currY >= 0 && currY < cells.size
                        && currX >= 0 && currX < cells[0].size) {
                    cells[currY][currX] = tetromino.color.ordinal + 1
                }
                currX++
            }
            currX = tetromino.x
            currY++
        }
    }

    fun shiftDown(): Int {
        val fullRows = mutableListOf<Int>()
        for ((index, row) in cells.withIndex()) {
            if (row.all { it != 0 }) {
                fullRows.add(index)
            }
        }

        for ((upTo, irow) in fullRows.withIndex()) {
            cells[irow].fill(0)
            bubbleUp(irow, upTo)
        }

        return fullRows.size
    }

    private fun bubbleUp(start: Int, to: Int) {
        var i = start
        while (i != to) {
            val curr = cells[i]
            val prev = cells[i - 1]
            cells[i] = prev
            cells[i - 1] = curr
            i--
        }
    }

    fun clear() {
        for (row in cells) {
            row.fill(0)
        }
    }
}