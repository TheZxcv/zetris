package it.thezxcv.zetris.model

object Constants {
    const val ROWS = 24
    const val COLUMNS = 10
}