package it.thezxcv.zetris

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import it.thezxcv.zetris.screen.LoadingScreen
import ktx.app.KtxGame
import ktx.app.KtxScreen
import ktx.freetype.registerFreeTypeFontLoaders
import ktx.inject.Context
import ktx.inject.register

class ZetrisGame : KtxGame<KtxScreen>() {
    private val context = Context()

    override fun create() {
        context.register {
//            bindSingleton(this@ZetrisGame)
            bindSingleton<Batch>(SpriteBatch())
            bindSingleton(ShapeRenderer())
            bindSingleton(BitmapFont().apply {
                data.scale(4f)
                region.texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
            })
            bindSingleton(AssetManager().apply {
                registerFreeTypeFontLoaders()
            })
            bindSingleton(OrthographicCamera())

            addScreen(LoadingScreen(this@ZetrisGame, inject(), inject(), inject(), inject()))
        }
        setScreen<LoadingScreen>()
        super.create()
    }

    override fun dispose() {
        context.dispose()
        super.dispose()
    }
}
