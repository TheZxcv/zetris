package it.thezxcv.zetris.renderer

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import it.thezxcv.zetris.assets.TextureAssets
import it.thezxcv.zetris.assets.TextureAtlasAssets
import it.thezxcv.zetris.assets.get
import it.thezxcv.zetris.model.Board
import it.thezxcv.zetris.model.Constants
import it.thezxcv.zetris.model.ShapeColor
import it.thezxcv.zetris.model.Tetromino

fun BitmapFont.drawCentre(batch: Batch, text: String, x: Float, y: Float) {
    val layout = GlyphLayout(this, text)
    draw(batch, text,
            x - (layout.width / 2f),
            y - (layout.height / 2f))
}

class ZetrisRenderer(assets: AssetManager,
                     private val x: Float,
                     private val y: Float,
                     val width: Float,
                     val height: Float) {
    private val background = assets[TextureAssets.Background].apply {
        setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
    }
    private val bricks = arrayOf(
            assets[TextureAtlasAssets.Game].findRegion("blue"),
            assets[TextureAtlasAssets.Game].findRegion("green"),
            assets[TextureAtlasAssets.Game].findRegion("pink"),
            assets[TextureAtlasAssets.Game].findRegion("purple"),
            assets[TextureAtlasAssets.Game].findRegion("yellow"),
    )

    private val cellWidth = width / Constants.COLUMNS
    private val cellHeight = height / Constants.ROWS

    fun renderBackground(batch: Batch) {
        val ratio = 1
        val uRight = width * ratio / background.width
        val vTop = height * ratio / background.height
        batch.draw(background,
                0f, 0f,
                width, height,
                0f, 0f,
                uRight, vTop);
    }

    fun render(board: Board, batch: Batch) {
        var currY = y + (height - cellHeight)
        for (row in board.cells) {
            renderLine(batch, x, currY, row)
            currY -= cellHeight
        }
    }

    fun render(tetromino: Tetromino, batch: Batch) {
        var currY = y + (height - (tetromino.y + 1) * cellHeight)
        for (row in tetromino.shape.shapes[tetromino.rotation]) {
            renderLine(batch, x + tetromino.x * cellWidth, currY, row, tetromino.color)
            currY -= cellHeight
        }
    }

    private fun renderLine(batch: Batch, posX: Float, posY: Float, row: IntArray, color: ShapeColor? = null) {
        var currX = posX
        for (cell in row) {
            if (cell != 0) {
                if (color != null) {
                    batch.draw(bricks[color.ordinal], currX, posY)
                } else {
                    batch.draw(bricks[cell - 1], currX, posY)
                }
            }
            currX += cellWidth
        }
    }
}