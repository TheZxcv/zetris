package it.thezxcv.zetris.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.StretchViewport
import it.thezxcv.zetris.assets.FontAssets
import it.thezxcv.zetris.assets.TextureAssets
import it.thezxcv.zetris.assets.get
import it.thezxcv.zetris.events.PeriodicEvent
import it.thezxcv.zetris.input.CustomGestureListener
import it.thezxcv.zetris.model.*
import it.thezxcv.zetris.renderer.ZetrisRenderer
import it.thezxcv.zetris.renderer.drawCentre
import ktx.app.KtxScreen
import ktx.graphics.use

private const val HEADER_BAR_HEIGHT = 128f

class GameScreen(
        private val batch: Batch,
        assets: AssetManager,
        private val camera: OrthographicCamera) : KtxScreen {
    private val shadow = assets[TextureAssets.Shadow].apply {
        setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
    }
    private val viewport = StretchViewport(64f * Constants.COLUMNS, 64f * Constants.ROWS + HEADER_BAR_HEIGHT, camera)
    private val touchPos = Vector3()

    private var gameover = false
    private var elapsedSinceGameOver = 0f
    private var activeTetromino = Tetromino(0, 0, TetrominoShape.I, ShapeColor.BLUE)
    private val board = Board()
    private val moveTimer = PeriodicEvent(0.15f)
    private var score = 0

    private val zetrisRenderer = ZetrisRenderer(assets, 0f, 0f, viewport.worldWidth, viewport.worldHeight - HEADER_BAR_HEIGHT)
    private val bigFont = assets[FontAssets.Big]
    private val smallFont = assets[FontAssets.Small]

    private fun spawnTetromino() {
        activeTetromino = Tetromino(4, 0, randomShape(), randomColor())
    }

    override fun render(delta: Float) {
        camera.update()

        batch.projectionMatrix = camera.projection
        batch.transformMatrix = camera.view
        batch.use {
            zetrisRenderer.renderBackground(it)
            zetrisRenderer.render(board, it)
            zetrisRenderer.render(activeTetromino, it)

            bigFont.drawCentre(it, "Score: $score",
                    camera.position.x,
                    zetrisRenderer.height + (HEADER_BAR_HEIGHT - bigFont.lineHeight / 2))

            if (gameover) {
                it.draw(shadow, 0f, 0f, viewport.worldWidth, viewport.worldHeight)

                bigFont.drawCentre(it, "GAME OVER!", camera.position.x, camera.position.y)
                if (elapsedSinceGameOver > 1) {
                    smallFont.drawCentre(it, "Press ANY key to restart", camera.position.x, camera.position.y - 3 * smallFont.lineHeight)
                }
            }
        }

        gameover = board.checkCollision(activeTetromino)
        if (!gameover) {
            handleInput()
            update(delta)
        } else {
            elapsedSinceGameOver += delta
            if (elapsedSinceGameOver > 1
                    && (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)
                            || Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)
                            || Gdx.input.justTouched())) {
                resetGame()
            }
        }
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height, true);
    }

    private fun update(delta: Float) {
        moveTimer.update(delta) { _ ->
            val newState = activeTetromino.down()
            val isLanded = board.checkCollision(newState)

            if (isLanded) {
                board.add(activeTetromino)
                val lines = board.shiftDown()
                score += if (lines == 4) {
                    1000
                } else {
                    lines * 100
                }

                spawnTetromino()
            } else {
                activeTetromino = newState
            }
        }
    }

    private fun handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
            tryLeft()
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            tryRight()
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            activeTetromino.rotate()
            tryRotate()
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            pushDown()
        }
    }

    private fun pushDown() {
        do {
            val newState = activeTetromino.down()
            val isValid = !board.checkCollision(newState);
            if (isValid) {
                activeTetromino = newState
            }
        } while (isValid)
    }

    private fun tryLeft() {
        val newState = activeTetromino.left()
        if (!board.checkCollision(newState)) {
            activeTetromino = newState
        }
    }

    private fun tryRight() {
        val newState = activeTetromino.right()
        if (!board.checkCollision(newState)) {
            activeTetromino = newState
        }
    }

    private fun tryRotate() {
        val newState = activeTetromino.rotate()
        if (!board.checkCollision(newState)) {
            activeTetromino = newState
        }
    }

    override fun show() {
        Gdx.input.inputProcessor = GestureDetector(CustomGestureListener().apply {
            onTap = { x, y, _, _ ->
                touchPos.set(x, y, 0f)
                camera.unproject(touchPos)

                when {
                    touchPos.y > camera.viewportHeight / 2f -> {
                        tryRotate()
                        true
                    }
                    touchPos.x > camera.viewportWidth / 2f -> {
                        tryRight()
                        true
                    }
                    touchPos.x <= camera.viewportWidth / 2f -> {
                        tryLeft()
                        true
                    }
                    else -> false
                }
            }

            onFling = { _, velocityY, _ ->
                if (velocityY > 0) {
                    pushDown()
                    true
                } else {
                    false
                }
            }
        })
        resetGame()
    }

    private fun resetGame() {
        board.clear()
        score = 0
        moveTimer.reset()
        gameover = false
        elapsedSinceGameOver = 0f
        spawnTetromino()
    }
}
