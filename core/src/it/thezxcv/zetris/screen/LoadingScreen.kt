package it.thezxcv.zetris.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.utils.viewport.StretchViewport
import it.thezxcv.zetris.ZetrisGame
import it.thezxcv.zetris.assets.*
import it.thezxcv.zetris.renderer.drawCentre
import ktx.app.KtxScreen
import ktx.graphics.use

class LoadingScreen(private val game: ZetrisGame,
                    private val batch: Batch,
                    private val font: BitmapFont,
                    private val assets: AssetManager,
                    private val camera: OrthographicCamera) : KtxScreen {
    private val logo = Texture(Gdx.files.internal("images/logo.png"))
    private val viewport = StretchViewport(logo.width.toFloat(), 2 * logo.height.toFloat(), camera)

    override fun render(delta: Float) {
        // continue loading our assets
        assets.update()
        camera.update()

        batch.projectionMatrix = camera.projection
        batch.transformMatrix = camera.view
        batch.use {
            it.draw(logo,
                    camera.position.x - (logo.width / 2f),
                    camera.position.y - (logo.height / 2f))

            if (assets.isFinished) {
                assets[FontAssets.Big].drawCentre(it, "Tap anywhere to begin!", camera.position.x, 150f)
            } else {
                font.drawCentre(it, "Loading assets...", camera.position.x, 150f)
            }
        }

        if (Gdx.input.isTouched && assets.isFinished) {
            // TODO: manage through DI
            game.addScreen(GameScreen(batch, assets, camera))
            game.setScreen<GameScreen>()
            game.removeScreen<LoadingScreen>()
            dispose()
        }
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height, true);
    }

    override fun show() {
        TextureAssets.values().forEach(assets::load)
        TextureAtlasAssets.values().forEach(assets::load)
        FontAssets.values().forEach(assets::load)
    }

    override fun dispose() {
        logo.dispose()
        super.dispose()
    }
}
