package it.thezxcv.zetris.assets

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader
import ktx.assets.getAsset
import ktx.assets.load

// texture atlas
enum class TextureAtlasAssets(val path: String) {
    Game("images/bricks.atlas")
}

inline fun AssetManager.load(asset: TextureAtlasAssets) = load<TextureAtlas>(asset.path)
inline operator fun AssetManager.get(asset: TextureAtlasAssets) = getAsset<TextureAtlas>(asset.path)

// texture
enum class TextureAssets(val path: String) {
    Background("images/background.png"),
    Shadow("images/shadow.png")
}

inline fun AssetManager.load(asset: TextureAssets) = load<Texture>(asset.path)
inline operator fun AssetManager.get(asset: TextureAssets) = getAsset<Texture>(asset.path)

// fonts
enum class FontAssets(val path: String, val parameters: FreetypeFontLoader.FreeTypeFontLoaderParameter) {
    Small("fonts/small.otf", FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
        fontFileName = "fonts/azonix.otf"
        fontParameters = FreeTypeFontGenerator.FreeTypeFontParameter().apply {
            size = 32
            borderWidth = 1f
            color = Color.WHITE
            borderColor = Color.BLACK
        }
    }),
    Big("fonts/big.otf", FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
        fontFileName = "fonts/azonix.otf"
        fontParameters = FreeTypeFontGenerator.FreeTypeFontParameter().apply {
            size = 48
            borderWidth = 1f
            color = Color.WHITE
            borderColor = Color.BLACK
        }
    }),
}

inline fun AssetManager.load(asset: FontAssets) = load<BitmapFont>(asset.path, asset.parameters)
inline operator fun AssetManager.get(asset: FontAssets) = getAsset<BitmapFont>(asset.path)
