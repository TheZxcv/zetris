package it.thezxcv.zetris.desktop

import com.badlogic.gdx.Application
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import it.thezxcv.zetris.ZetrisGame

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration().apply {
            title = "Zetris"
            width = 720
            height = 1280
        }
        LwjglApplication(ZetrisGame(), config).logLevel = Application.LOG_DEBUG
    }
}
